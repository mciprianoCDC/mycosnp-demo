# mycosnp-demo

## Introduction

## Requirements
python

singularity


## Install
This document will walk you through setting up and running mycosnp.

### Create Directories
```bash
mkdir ~/mycosnp
cd ~/mycosnp
```

### Install geneflow2

```bash
python3 -m venv gfpy
source gfpy/bin/activate
pip3 install geneflow
```

### Install workflows
```bash
gf install-workflow --make-apps -f -g https://github.com/CDCgov/mycosnp-bwa-reference mycosnp-bwa-reference/0.21
 
gf install-workflow --make-apps -f -g https://github.com/CDCgov/mycosnp-bwa-pre-process mycosnp-bwa-pre-process/0.21
  
gf install-workflow --make-apps -f -g https://github.com/CDCgov/mycosnp-gatk-variants mycosnp-gatk-variants/0.21
```

### Download demo data.
```bash
git clone https://gitlab.com/mciprianoCDC/mycosnp-demo.git
```
## Testing
The below commands expect the Install done above within the ~/mycosnp directory. If different, replace appropriate directories. The code here will use the files within the mycosnp-demo directory for each step rather than the results from the previous step for testing functionality.

### Setup
The below would need to be run before to setup the environment before running mycosnp.

```bash
cd ~/mycosnp
source gfpy/bin/activate
```
### Prepare Reference Files

```bash
gf --log-level debug run mycosnp-bwa-reference/0.21 \
    -o ./output \
    -n test-mycosnp-bwa-reference \
    --in.reference_sequence ~/mycosnp/mycosnp-demo/mycosnp-bwa-reference/data/c_auris_test.fasta  \
    --param.threads 4
```

### Preprocess (Mapping and QC)

```bash
gf --log-level debug run mycosnp-bwa-pre-process/0.21 \
    -o ./output \
    -n test-mycosnp-bwa-pre-process \
    --in.input_folder ~/mycosnp/mycosnp-demo/mycosnp-bwa-pre-process/data/fastq \
    --in.reference_index ~/mycosnp/mycosnp-demo/mycosnp-bwa-pre-process/data/bwa_index \
    --in.reference_sequence ~/mycosnp/mycosnp-demo/mycosnp-bwa-pre-process/data/reference/c_auris_test.fasta \
        --param.rate 1.0 \
        --param.threads 4
```


### Variant Calling (GATK)

```bash
gf --log-level debug run mycosnp-gatk-variants/0.21 \
    -o ./output \
    -n test-mycosnp-gatk-variants \
    --in.input_folder ~/mycosnp/mycosnp-demo/mycosnp-gatk-variants/data/bam_index \
    --in.reference_sequence ~/mycosnp/mycosnp-demo/mycosnp-gatk-variants/data/indexed_reference \
    --param.max_perc_amb_samples 10 \
    --param.pair_hmm_threads 8
```

## Running on SGE Cluster

### Options
 Add these options for the above commands. Slots should match the number of threads given to a process.

```bash
export DRMAA_LIBRARY_PATH=/opt/sge/lib/lx-amd64/libdrmaa2.so


--ec default:gridengine \
--ep \
   default.slots:4 \
   'default.queue:all.q@@sd' \
   'default.init:echo `hostname` && mkdir -p $HOME/tmp && export TMPDIR=$HOME/tmp && export _JAVA_OPTIONS=-Djava.io.tmpdir=$HOME/tmp && export PATH=/usr/sbin:$PATH && export XDG_RUNTIME_DIR='

```

### Example SGE script
```bash
#!/bin/bash -l
#$ -q all.q
#$ -cwd

source ~/mycosnp/gfpy/bin/activate
export GENEFLOW_PATH=~/mycosnp
export DRMAA_LIBRARY_PATH=/opt/sge/lib/lx-amd64/libdrmaa2.so


gf --log-level=debug run mycosnp-bwa-pre-process/0.21 \
    -o ./output \
    -n test-mycosnp-bwa-preprocess-sge \
    --in.input_folder ~/mycosnp/mycosnp-demo/mycosnp-bwa-pre-process/data/fastq \
    --in.reference_index ~/mycosnp/mycosnp-demo/mycosnp-bwa-pre-process/data/bwa_index \
    --in.reference_sequence ~/mycosnp/mycosnp-demo/mycosnp-bwa-pre-process/data/reference/c_auris_test.fasta \
    --param.threads 4 \
    --ec default:gridengine \
    --ep \
        default.slots:4 \
       'default.init:echo `hostname` && mkdir -p $HOME/tmp && export TMPDIR=$HOME/tmp && export _JAVA_OPTIONS=-Djava.io.tmpdir=$HOME/t
mp && export PATH=/usr/sbin:$PATH && export XDG_RUNTIME_DIR=' \

```

### Example Run From Start to Finish
The below uses specific output directories. Geneflow creates a random hash which is included in the output directory, so you will need to look at what that is when you run the commands after it finishes and change the appropriate directories below.

```bash

cd ~/mycosnp
source gfpy/bin/activate
export GENEFLOW_PATH=~/mycosnp


gf --log-level debug run mycosnp-bwa-reference/0.21 \
    -o ./output \
    -n testdata-mycosnp-bwa-reference \
    --in.reference_sequence ~/mycosnp/mycosnp-demo/test-data/reference/c_auris_test.fasta \
    --param.threads 1

gf --log-level debug run mycosnp-bwa-pre-process/0.21 \
    -o ./output \
    -n testdata-mycosnp-bwa-pre-process \
    --in.input_folder ~/mycosnp/mycosnp-demo/test-data/data \
    --in.reference_index ~/mycosnp/output/testdata-mycosnp-bwa-reference-04fc8217/bwa_index/bwa_index \
    --in.reference_sequence ~/mycosnp/mycosnp-demo/test-data/reference/c_auris_test.fasta \
        --param.rate 1.0 \
        --param.threads 4

gf --log-level debug run mycosnp-gatk-variants/0.21 \
    -o ./output \
    -n testdata-mycosnp-gatk-variants \
    --in.input_folder ~/mycosnp/output/testdata-mycosnp-bwa-pre-process-664f50e3/bam_index \
    --in.reference_sequence ~/mycosnp/output/testdata-mycosnp-bwa-reference-04fc8217/index_reference/indexed_reference/ \
    --param.max_perc_amb_samples 10 \
    --param.pair_hmm_threads 4


```

### Explore Output

```bash

cd ~/mycosnp
tree output

# Additional files are kept in a geneflow work directory. 
# This directory can be explored for intermediate files.
# If the workflow fails to run, this directory will contain
# logs of all the processes.
# NOTE: This directory can be deleted while no geneflow process are being run
# without issue, and it is recommended to clean out this directory periodically as
# it can get large.

tree ~/.geneflow

```
### Files created

```
├── testdata-mycosnp-bwa-reference-f953bb7f
│   ├── bwa_index
│   │   ├── bwa_index
│   │   │   ├── bwa_index.amb
│   │   │   ├── bwa_index.ann
│   │   │   ├── bwa_index.bwt
│   │   │   ├── bwa_index.pac
│   │   │   └── bwa_index.sa
│   │   └── _log
│   │       ├── bwa_index-bwa-index.stderr
│   │       ├── bwa_index-bwa-index.stdout
│   │       ├── gf-0-bwa_index-bwa_index.err
│   │       └── gf-0-bwa_index-bwa_index.out
│   └── index_reference
│       ├── indexed_reference
│       │   ├── indexed_reference.dict
│       │   ├── indexed_reference.fasta
│       │   └── indexed_reference.fasta.fai
│       └── _log
│           ├── gf-0-index_reference-indexed_reference.err
│           ├── gf-0-index_reference-indexed_reference.out
│           ├── indexed_reference-picard-createsequencedictionary.stderr
│           ├── indexed_reference-picard-createsequencedictionary.stdout
│           ├── indexed_reference-samtools-faidx.stderr
│           └── indexed_reference-samtools-faidx.stdout



├── testdata-mycosnp-gatk-variants-0efef2d9
│   ├── consensus
│   │   ├── consensus
│   │   │   ├── SRR13710812.fasta.gz
│   │   │   └── SRR14802833.fasta.gz
│   │   └── _log
│   │       ├── consensus-bcftools-consensus.stderr
│   │       ├── consensus-bcftools-index.stderr
│   │       ├── consensus-bcftools-index.stdout
│   │       ├── consensus-bgzip.stderr
│   │       ├── gf-0-consensus-consensus.err
│   │       └── gf-0-consensus-consensus.out
│   ├── gatk-selectvariants
│   │   ├── gatk-selectvariants
│   │   │   ├── gatk-selectvariants-clean.vcf
│   │   │   ├── gatk-selectvariants-clean.vcf.gz
│   │   │   ├── gatk-selectvariants-clean.vcf.gz.csi
│   │   │   ├── gatk-selectvariants.vcf
│   │   │   └── gatk-selectvariants.vcf.idx
│   │   └── _log
│   │       ├── gatk-selectvariants-gatk-selectvariants.stderr
│   │       ├── gatk-selectvariants-gatk-selectvariants.stdout
│   │       ├── gf-0-gatk-selectvariants-gatk-selectvariants.err
│   │       └── gf-0-gatk-selectvariants-gatk-selectvariants.out
│   ├── split-vcf-broad
│   │   ├── _log
│   │   │   ├── gf-0-split-vcf-broad-split-vcf-broad.err
│   │   │   ├── gf-0-split-vcf-broad-split-vcf-broad.out
│   │   │   ├── split-vcf-broad-bcftools-index.stderr
│   │   │   ├── split-vcf-broad-bcftools-index.stdout
│   │   │   ├── split-vcf-broad-bcftools-query.stderr
│   │   │   └── split-vcf-broad-bgzip.stderr
│   │   └── split-vcf-broad
│   │       ├── SRR13710812.vcf.gz
│   │       └── SRR14802833.vcf.gz
│   ├── split-vcf-selectvariants
│   │   ├── _log
│   │   │   ├── gf-0-split-vcf-selectvariants-split-vcf-selectvariants.err
│   │   │   ├── gf-0-split-vcf-selectvariants-split-vcf-selectvariants.out
│   │   │   ├── split-vcf-selectvariants-bcftools-index.stderr
│   │   │   ├── split-vcf-selectvariants-bcftools-index.stdout
│   │   │   ├── split-vcf-selectvariants-bcftools-query.stderr
│   │   │   └── split-vcf-selectvariants-bgzip.stderr
│   │   └── split-vcf-selectvariants
│   │       ├── SRR13710812.vcf.gz
│   │       └── SRR14802833.vcf.gz
│   ├── vcf-filter
│   │   ├── _log
│   │   │   ├── gf-0-vcf-filter-vcf-filter.err
│   │   │   └── gf-0-vcf-filter-vcf-filter.out
│   │   └── vcf-filter
│   │       ├── vcf-filter.stats.txt
│   │       ├── vcf-filter.vcf
│   │       ├── vcf-filter.vcf.gz
│   │       └── vcf-filter.vcf.gz.csi
│   └── vcf-to-fasta
│       ├── _log
│       │   ├── gf-0-vcf-to-fasta-vcf-to-fasta.err
│       │   ├── gf-0-vcf-to-fasta-vcf-to-fasta.out
│       │   └── vcf-to-fasta-broad-vcf-to-fasta.stderr
│       └── vcf-to-fasta
│           └── vcf-to-fasta.fasta



├── testdata-mycosnp-bwa-pre-process-30f67c9d
│   ├── bam_index
│   │   ├── _log
│   │   │   ├── gf-0-bam_index-srr13710812.err
│   │   │   ├── gf-0-bam_index-srr13710812.out
│   │   │   ├── gf-0-bam_index-srr14802833.err
│   │   │   ├── gf-0-bam_index-srr14802833.out
│   │   │   ├── SRR13710812-samtools-index.stderr
│   │   │   ├── SRR13710812-samtools-index.stdout
│   │   │   ├── SRR14802833-samtools-index.stderr
│   │   │   └── SRR14802833-samtools-index.stdout
│   │   ├── SRR13710812
│   │   │   ├── SRR13710812.bam
│   │   │   └── SRR13710812.bam.bai
│   │   └── SRR14802833
│   │       ├── SRR14802833.bam
│   │       └── SRR14802833.bam.bai
│   ├── qc_report
│   │   ├── _log
│   │   │   ├── gf-0-qc_report-qc_report-txt.err
│   │   │   └── gf-0-qc_report-qc_report-txt.out
│   │   ├── qc_report.txt
│   │   └── _tmp
│   │       ├── SRR13710812_qc_report.pdf.txt
│   │       └── SRR14802833_qc_report.pdf.txt
│   └── qc_trim
│       ├── _log
│       │   ├── gf-0-qc_trim-srr13710812.err
│       │   ├── gf-0-qc_trim-srr13710812.out
│       │   ├── gf-0-qc_trim-srr14802833.err
│       │   ├── gf-0-qc_trim-srr14802833.out
│       │   ├── SRR13710812-faqcs.stderr
│       │   ├── SRR13710812-faqcs.stdout
│       │   ├── SRR14802833-faqcs.stderr
│       │   └── SRR14802833-faqcs.stdout
│       ├── SRR13710812
│       │   ├── SRR13710812.1.trimmed.fastq
│       │   ├── SRR13710812.2.trimmed.fastq
│       │   ├── SRR13710812_qc_report.pdf
│       │   ├── SRR13710812.stats.txt
│       │   └── SRR13710812.unpaired.trimmed.fastq
│       └── SRR14802833
│           ├── SRR14802833.1.trimmed.fastq
│           ├── SRR14802833.2.trimmed.fastq
│           ├── SRR14802833_qc_report.pdf
│           ├── SRR14802833.stats.txt
│           └── SRR14802833.unpaired.trimmed.fastq

```
